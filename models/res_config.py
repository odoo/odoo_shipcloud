from openerp import fields, models, api
import logging

_logger = logging.getLogger(__name__)

class website_config_settings(models.Model):
    _inherit = 'website.config.settings'
    _name = 'website.sc.config.settings'

    sc_api_key_sandbox = fields.Char('shipcloud Sandbox API key')
    sc_api_key_prod = fields.Char('shipcloud Production API key', required=1)
    sc_api_use_prod = fields.Boolean('use shipcloud Production API key')

    @api.model
    def get_default_sc_values(self, fields):
        ir_values = self.env['ir.values']
        sc_config_values_list_tuples = ir_values.get_defaults('delivery.carrier')
        sc_config_values = {}
        for item in sc_config_values_list_tuples:
            sc_config_values.update({item[1]:item[2]})
        return sc_config_values

    @api.one
    def set_sc_values(self):
        ir_values = self.env['ir.values']
        for config in self:
            ir_values.set_default('delivery.carrier', 'sc_api_key_sandbox', config.sc_api_key_sandbox or '')
            ir_values.set_default('delivery.carrier', 'sc_api_key_prod', config.sc_api_key_prod or '')
            ir_values.set_default('delivery.carrier', 'sc_api_use_prod', config.sc_api_use_prod or False)

        return True
