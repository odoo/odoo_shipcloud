from openerp import api, fields, models

# extend deliver.carrier with shipcloud
class SMCShippingShipcloud(models.Model):
    _inherit = 'delivery.carrier'
    delivery_type = fields.Selection(selection_add=[('sc', 'shipcloud')])
    sc_carrier_service = fields.Many2one(comodel_name='delivery.carrier.sc.carrier_service',
                                         string='Shipcloud Carrier Service')

# extend stock.picking with fields related to shipcloud
class SMCStockPickingShipclodu(models.Model):
    _inherit = 'stock.picking'
    sc_shipment_id = fields.Char(string='shipcloud shipment ID', readonly=True, copy=False)
    sc_tracking_url = fields.Char(string='shipcloud tracking URL', readonly=True, copy=False)
    sc_content_desc = fields.Char(string='Description of Contents')


class SMCShippingScCarrierService(models.Model):
    _name = 'delivery.carrier.sc.carrier_service'
    name = fields.Char(string="Name", required=1)
    carrier = fields.Char(string="shipcloud Carrier", required=1)
    service = fields.Char(string="shipcloud Service", required=1)
    label_size = fields.Char(string="shipcloud Label Size", required=1)
