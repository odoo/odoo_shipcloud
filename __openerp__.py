{
    'name': 'shipcloud.io Shipping Integration',
    'category': 'Website/Shipping Logistics',
    'summary': 'Integrate shipping via shipcloud.io directly within Odoo',
    'website': 'https://sysmocom.de/',
    'version': '0.2',
    'description':"""
    """,
    'author': 'Harald Welte',
    'depends': ['odoo_shipping_service_apps', 'shipment_packaging'],
    'data': [
        'security/ir.model.access.csv',
        'views/sc_delivery_carrier.xml',
        'views/shipcloud_pickup.xml',
        'views/res_config.xml',
        'data/data.xml',
    ],
    'installable': True,
    'application': True,
    'external_dependencies': {
        #'python': ['inema']
    },
}
